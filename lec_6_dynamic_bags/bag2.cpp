// FILE: bag2.cpp
// CLASS implemented: bag (see bag2.h for documentation)
// INVARIANT for the bag class:
//   1. The number of items in the bag is in the member variable m_used;
//   2. The actual items of the bag are stored in a partially filled array.
//      The array is a dynamic array, pointed to by the member variable m_data;
//   3. The size of the dynamic array is in the member variable m_capacity.

#include <algorithm>    // Provides copy function
#include <cassert>       // Provides assert function
#include "bag2.h"
using namespace std;

namespace main_savitch_4
{
    const bag::size_type bag::DEFAULT_CAPACITY;

    bag::bag(size_type initial_capacity)
    {
        m_data = new value_type[initial_capacity];
        m_capacity = initial_capacity;
        m_used = 0;
    }

    bag::bag(const bag& source)
    // Library facilities used: algorithm
    {
        m_data = new value_type[source.m_capacity];
        m_capacity = source.m_capacity;
        m_used = source.m_used;
        copy(source.m_data, source.m_data + m_used, m_data);
    }

    bag::~bag( )
    {
	delete [ ] m_data;
    }

    void bag::reserve(size_type new_capacity)
    // Library facilities used: algorithm
    {
        value_type *larger_array;

        if (new_capacity == m_capacity)
            return; // The allocated memory is already the right size.

        if (new_capacity < m_used)
            new_capacity = m_used; // Can't allocate less than we are using.

        larger_array = new value_type[new_capacity];
        copy(m_data, m_data + m_used, larger_array);
        delete [ ] m_data;
        m_data = larger_array;
        m_capacity = new_capacity;
    }
    
    bag::size_type bag::erase(const value_type& target)
    {
	size_type index = 0;
	size_type many_removed = 0;

	while (index < m_used)
	{
	    if (m_data[index] == target)
	    {
		--m_used;
		m_data[index] = m_data[m_used];
		++many_removed;
	    }
	    else
		++index;
	}

	return many_removed;
    }

    bool bag::erase_one(const value_type& target)
    {
	size_type index; // The location of target in the m_data array    

	// First, set index to the location of target in the m_data array,
	// which could be as small as 0 or as large as m_used-1.
	// If target is not in the array, then index will be set equal to m_used.
	index = 0;
	while ((index < m_used) && (m_data[index] != target))
	    ++index;

	if (index == m_used) // target isn't in the bag, so no work to do
	    return false;

	// When execution reaches here, target is in the bag at m_data[index].
	// So, reduce m_used by 1 and copy the last item onto m_data[index].
	--m_used;
	m_data[index] = m_data[m_used];
	return true;
    }

    void bag::insert(const value_type& entry)
    {   
        if (m_used == m_capacity)
            reserve(m_used+1);
        m_data[m_used] = entry;
        ++m_used;
    }
    
    void bag::operator +=(const bag& addend)
    // Library facilities used: algorithm
    {
        if (m_used + addend.m_used > m_capacity)
            reserve(m_used + addend.m_used);
        
        copy(addend.m_data, addend.m_data + addend.m_used, m_data + m_used);
	m_used += addend.m_used;
    }

    void bag::operator =(const bag& source)
    // Library facilities used: algorithm
    {
	value_type *new_m_data;

	// Check for possible self-assignment:
	if (this == &source)
            return;

	// If needed, allocate an array with a different size:
	if (m_capacity != source.m_capacity)
	{ 
	    new_m_data = new value_type[source.m_capacity];
	    delete [ ] m_data;
	    m_data = new_m_data;
	    m_capacity = source.m_capacity;
	}

	// Copy the m_data from the source array:
	m_used = source.m_used;
	copy(source.m_data, source.m_data + m_used, m_data);
    }

    bag::size_type bag::count(const value_type& target) const
    {
	size_type answer;
	size_type i;

	answer = 0;
	for (i = 0; i < m_used; ++i)
	    if (target == m_data[i])
		++answer;
	return answer;
    }

    bag operator +(const bag& b1, const bag& b2)
    {
	bag answer(b1.size( ) + b2.size( ));

	answer += b1; 
	answer += b2;
	return answer;
    }

}
